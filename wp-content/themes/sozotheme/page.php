<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="col-md-8 left-content">
					<div id="main-content" class="page <?php if (get_the_id() == 21) echo 'contact-page'; ?>">
						<div class="page-title"><?php echo get_the_title(); ?></div>
						<div class="page-content">
							<?php the_content(); ?>
						</div>
					</div>		
				</div>	<!-- /main-content -->

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
