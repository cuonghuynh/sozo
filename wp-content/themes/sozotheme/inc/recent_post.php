<?php 

	$args = array(
		'numberposts' 	=> 4,
		'orderby' 		=> 'post_date',
    	'order' 		=> 'DESC',
    	'post_type' 	=> 'post',
    	);

	$recent_posts = wp_get_recent_posts( $args, OBJECT );

	//var_dump($recent_posts);
?>
					<?php foreach ($recent_posts as $key => $post): ?>
							<div class="item">
								<a class="item-title" href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a>
								<span class="item-date"> <?php echo  date('F d, Y', strtotime($post->post_date));  ?></span>
							</div>
					<?php endforeach ?>
