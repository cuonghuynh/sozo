<?php 

	$args = array(
		'showposts' 		=> 4,
		'orderby' 			=> 'date',
    	'order' 			=> 'DESC',
    	'post_type' 		=> 'post',
    	'ignore_sticky_posts' => true,
    	);

	$query = new WP_Query( $args );

	if ( $query->have_posts()): 
		while ( $query->have_posts()): $query->the_post(); 

		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'post-thumb-widget', true);
?>
										<div class="item">
											<div class="item-image">
												<a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb_url[0]; ?>" alt=""></a>
											</div>
											<div class="item-title">
												<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											</div>
											<div class="item-date">
												<?php the_time( 'F d, Y' ); ?>
											</div>
										</div>
<?php 
	endwhile;
else: ?>

	<p>No article found.</p>

<?php endif; 
	wp_reset_query();
?>