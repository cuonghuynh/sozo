
<div class="row post-pagination">
	<?php 
		$prev_post = get_previous_post();
	?>
	<?php if (is_a($prev_post, 'WP_Post')): ?>
		<div class="col-sm-6 pagination-nav">
			<a href="<?php echo get_permalink($prev_post->ID); ?>" class="prev">
			<span>« PREVIOUS POST</span>
			<?php echo get_the_title( $prev_post->ID ); ?>
			</a>
		</div>
	<?php endif ?>

	<?php 
		$next_post = get_next_post();
	?>
	<?php if (is_a($next_post, 'WP_Post')): ?>
		<div class="col-sm-6 pagination-nav">	
			<a href="<?php echo get_permalink($next_post->ID ); ?>" class="next">
			<span>NEXT POST »</span>
			<?php echo get_the_title( $next_post->ID ); ?>
			</a>
		</div>
	<?php endif ?>
	
</div>