<?php
/**
 * Google plus badge Widget Class
 */
class google_badge_widget extends WP_Widget {
 
 
		/** constructor -- name this the same as the class above */
		function google_badge_widget() {
				parent::WP_Widget(false, $name = 'Google+ Badge Widget');	
		}
 
		/** @see WP_Widget::widget -- do not rename this */
		/**
		 * HTML display in template
		 * @param  array $args     Store $before_widget, $after_widget, $before_title, $after_title HTML
		 * @param  array $instance Store fields
		 * @return avoid
		 */
		function widget($args, $instance) {	
				extract( $args );		
				$title 		= apply_filters('widget_title', $instance['title']);
				$url 	= $instance['url'];
				?>
							<?php echo $before_widget; ?>
									<?php if ( $title )
												echo $before_title . $title . $after_title; ?>
									<div class="g-page" data-width="200px" data-href="<?php echo $url ?>" data-layout="landscape" data-theme="light"></div>
							<?php echo $after_widget; ?>
				<?php
		}
 
		/** @see WP_Widget::update -- do not rename this */
		/**
		 * Update item value in database
		 * @param  array $new_instance Store new value
		 * @param  array $old_instance Store old value
		 * @return array
		 */
		function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['url'] = strip_tags($new_instance['url']);
				return $instance;
		}
 
		/** @see WP_Widget::form -- do not rename this */
		/**
		 * This Form display in Admin page
		 * @param  array $instance
		 * @return avoid
		 */
		function form($instance) {	
 
				$title 		= esc_attr($instance['title']);
				$url	= esc_attr($instance['url']);
				?>
				 <p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
					<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
				</p>
		<p>
					<label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Google+ Badge URL'); ?></label> 
					<input class="widefat" id="<?php echo $this->get_field_id('url'); ?>" name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php echo $url; ?>" />
				</p>
				<?php 
		}
 
 
} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("google_badge_widget");'));
?>
