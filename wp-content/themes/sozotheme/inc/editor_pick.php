<?php 
	
	//get all sticky posts
	$sticky = get_option( 'sticky_posts' );

	//sort the stickies with newest ones at the top
	rsort($sticky);

	//get the 4 sticky
	$sticky = array_slice($sticky, 0, 4);
	
	//query sticky posts
	$query = new WP_Query( array( 'post__in' => $sticky , 'ignore_sticky_posts' => 1 ) );

	//var_dump($query);
	
	if ( $query->have_posts()): 
		while ( $query->have_posts()): $query->the_post(); 

		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'post-thumb-widget', true);

?>
								<div class="item">
									<div class="item-image">
										<a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb_url[0]; ?>" alt=""></a>
									</div>
									<div class="item-title">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
									<div class="item-date">
										<?php the_time( 'F d, Y' ); ?>
									</div>
								</div>
<?php 
	endwhile;
else: ?>

	<p>No article found.</p>

<?php endif; 
	wp_reset_query();
?>