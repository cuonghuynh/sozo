<?php 
	get_header(); 

	$paged = 1;
	$args = array(
		'posts_per_page' 	=> 10,
		'paged'				=> $paged,
        'post_type'         => 'post',
        'orderby'			=> 'date',
        'order'           	=> 'desc',
        'category__not_in'	=> array( 1 ),
        'ignore_sticky_posts' => true,
        );

	$query = new WP_Query( $args );


?>

				<div class="col-md-8 left-content">
					<div id="main-content">

<?php
	if ( $query->have_posts()): 

		$counter=1;

		while ( $query->have_posts()): $query->the_post(); 

		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src( $thumb_id, 'full');

		$image = aq_resize( $thumb_url[0], 420 , 195 , true ); //resize & crop img

?>

				

						<div class="row item <?php if (is_sticky()) echo 'featured'; ?>">
							<div class="col-sm-8 item-image">
								<img src="<?php echo $image; ?>" alt="">
							</div>
							<div class="col-sm-4 item-detail">
								<div class="item-title">
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								</div>
								<div class="item-excerpt">
									<?php the_excerpt(); ?>
								</div>
								<div class="item-read-more">
									<a href="<?php the_permalink(); ?>">READ ARTICLE &rarr;</a>
								</div>
								<div class="item-tag">
									<ul>
									<?php 
									$tags = get_the_tags();
									?>
									<?php 
									  if (!empty($tags)) :
									  foreach ($tags as $key => $tag): 
									  $tag_link = get_tag_link( $tag->term_id );
									?>
									  <li><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a></li>
									<?php endforeach; endif; ?>
									</ul>
								</div>
							</div>
						</div> <!-- /item -->

						<?php if ($counter++ % 5 == 0): ?>
							<div class="adverts-item">
								<?php dynamic_sidebar( 'Post Content Sidebar' ); ?>
							</div>
						<?php endif ?>
						

<?php endwhile; else: ?>

						<p>No article found.</p>

<?php endif; ?>

					</div>
					
					<div id="load-more-post" data-paged="<?php echo $paged; ?>" data-cat="all">
						<h4><img src="<?php echo get_bloginfo('template_url'); ?>/images/ajax-loader.gif" alt="">Load more posts</h4>
					</div>
				</div>	<!-- /main-content -->

<?php
	wp_reset_query();
	get_footer(); 

?>