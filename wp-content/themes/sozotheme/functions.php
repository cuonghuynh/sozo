<?php
	require_once( TEMPLATEPATH . '/inc/aq_resizer.php' );
  // Add RSS links to <head> section
  automatic_feed_links();

  // UNCOMMENT THE FOLLOWING IF YOU NEED JQUERY LOADED:
  /*
  if ( !is_admin() ) {
	  wp_deregister_script('jquery');
	  wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"), false);
	  wp_enqueue_script('jquery');
  }
  */

  // Clean up the <head>
  function removeHeadLinks() {
	 remove_action('wp_head', 'rsd_link');
	 remove_action('wp_head', 'wlwmanifest_link');
  }

  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');

  function blankhtml5_comment($comment, $args, $depth) {
	 $GLOBALS['comment'] = $comment; ?>
	 <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<article id="comment-<?php comment_ID(); ?>">
		  <header class="comment-author vcard">
			 <?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>

			 <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
			 <?php if ($comment->comment_approved == '0') : ?>
				<em><?php _e('Your comment is awaiting moderation.') ?></em><br />
			 <?php endif; ?>

			 <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
		  </header>

		  <?php comment_text() ?>

		  <div class="reply">
			 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		  </div>
		</article>
  <?php
  }

  // ####################################    my functions    #######################################

  /**
	* Add thumbnail to page editor
	*/
	if (function_exists('add_theme_support')) {
		add_theme_support( 'post-thumbnails', array('post', 'page') );
	}

	/**
	 *  Register Page Navigation Menu
	 */
	if(function_exists('register_nav_menus')){
		register_nav_menus (array(
			'page_menu' => 'Page Navigation Menu',
			'cat_menu'  => 'Category Navigation Menu',
			));
	}

	add_action( 'init' , 'my_add_excerpt_to_pages' );
	/**
	 * Add post type support to define excerpt content to pages
	 * @return 
	 */
	function my_add_excerpt_to_pages()
	{
		add_post_type_support( 'page' , 'excerpt' );
	}

	add_action( 'after_setup_theme' , 'my_add_image_size' );
	/**
	 * Add new image size
	 * @return 
	 */
	function my_add_image_size()
	{
		add_image_size( 'post-thumb-normal', 420 , 195 , true );
		add_image_size( 'post-thumb-widget', 70 , 70 , true );
	}

	add_filter( 'image_size_names_choose', 'my_custom_sizes' );
	/**
	 * Add new image size option to the list of selectable sizes in Media Library
	 * @param  [type] $sizes [description]
	 * @return [type]        [description]
	 */
	function my_custom_sizes( $sizes ) {
	    return array_merge( $sizes, array(
	        'post-thumb-normal' => __('Homepage Thumb (420x195)'),
	        'post-thumb-widget' => __('Widget Thumb (70x70)'),
	    ) );
	}

	/**
	 * Modify excerpt length to new size
	 * @param  [integer] $length [description]
	 * @return 
	 */
	function new_excerpt_len($length){
		return 20;
	}
	add_filter('excerpt_length', 'new_excerpt_len');

	function new_excerpt_more($more){
		global $post;
		return '';
	}
	add_filter('excerpt_more', 'new_excerpt_more');


	if (function_exists('register_sidebar')) {
	  	register_sidebar(array(
	  		'name' => 'Banner Sidebar',
	  		'id'   => 'banner-sidebar',
	  		'description'   => 'This sidebar only for advertisment.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<h1>',
	  		'after_title'   => '</h1>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Right Sidebar',
	  		'id'   => 'right-sidebar',
	  		'description'   => 'This sidebar only for advertisment.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<h1>',
	  		'after_title'   => '</h1>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Post Content Sidebar',
	  		'id'   => 'post-content-sidebar',
	  		'description'   => 'This sidebar only for advertisment.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<h1>',
	  		'after_title'   => '</h1>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Sidebar Widgets',
	  		'id'   => 'sidebar-widgets',
	  		'description'   => 'These are widgets for the sidebar.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="title">',
	  		'after_title'   => '</div>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Popular Post Widgets',
	  		'id'   => 'popular-post-widgets',
	  		'description'   => 'The widget display popular posts (in day).',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="title">',
	  		'after_title'   => '</div>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Best Of Afachan Widgets',
	  		'id'   => 'best-post-widgets',
	  		'description'   => 'The widget display best posts (in all time).',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="title">',
	  		'after_title'   => '</div>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Fanpage Bottom Sidebar',
	  		'id'   => 'fanpage-bottom-sidebar',
	  		'description'   => 'The widget display fanpage.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="title">',
	  		'after_title'   => '</div>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Facebook Comment Sidebar',
	  		'id'   => 'facebook-comment-sidebar',
	  		'description'   => 'The widget display Facebook comment.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="title">',
	  		'after_title'   => '</div>'
	  		));

	  	register_sidebar(array(
	  		'name' => 'Author Sidebar',
	  		'id'   => 'author-sidebar',
	  		'description'   => 'The widget display Starbox plugin.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="title">',
	  		'after_title'   => '</div>'
	  		));
	  	register_sidebar(array(
	  		'name' => 'Twitter Feed Sidebar',
	  		'id'   => 'twitter-feed-sidebar',
	  		'description'   => 'The widget display Twitter Feed plugin.',
	  		'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  		'after_widget'  => '</div>',
	  		'before_title'  => '<div class="widget-title">',
	  		'after_title'   => '</div>'
	  		));
  	}

  	include( TEMPLATEPATH . '/inc/fanpage_plugin.php' );
  	include( TEMPLATEPATH . '/inc/badge_plugin.php' );
  	include( TEMPLATEPATH . '/inc/comment_plugin.php' );
  	
  	/**
  	 * Add javascript send ajax request to admin-AJAX.php into wp_footer
  	 */
  	add_action( 'wp_footer', 'load_more_javascript' );
    function load_more_javascript() {
?>
        <!--Ajax script -->
        <script>// <![CDATA[
        $(document).ready(function($) {
        	$('#load-more-post').click(function(event) {
        		var paged = $(this).data('paged') + 1;
        		var cat = $(this).data('cat');
        		var author = $(this).data('author');
        		$('#load-more-post img').css('display', 'inline-block');
        		$.ajax({
        			url: '<?php echo admin_url( "admin-AJAX.php" ); ?>',
        			type: 'POST',
        			dataType: 'html',
        			data: {'action' : 'all_category', 'paged' : paged, 'cat' : cat, 'author' : author},
        			success: function(result) {
        				if (result != "") {
        					$('#main-content').append(result);
        					$('#load-more-post').data('paged', paged);
        				} else {
        					$('#load-more-post').find('h4').html('No more posts');
        					$('#load-more-post').attr('disabled','disabled');
        				}
        				$('#load-more-post img').css('display', 'none');
        			},
        		});
        		
        	});
            
        });
	// ]]></script>

<?php  }

	// Register function handles ajax request
	// wp_AJAX_{action-name} : User logged in
	add_action('wp_ajax_all_category', 'get_all_category');
	// wp_AJAX_nopriv_{action-name} : User not log in
	add_action('wp_ajax_nopriv_all_category', 'get_all_category');
	
	function get_all_category() {

		$paged = $_POST['paged'];
		$cat = $_POST['cat'];
		$author = $_POST['author'];

		if (!empty($cat)) {

			if ($cat == 'all') {

				$args = array(
					'posts_per_page' 		=> 10,
					'paged'					=> $paged,
			        'post_type'         	=> 'post',
			        'orderby'				=> 'date',
			        'order'           		=> 'desc',
			        'category__not_in'		=> array( 1 ),
			        'ignore_sticky_posts' 	=> true,
		        );

			} else {

				$args = array(
					'posts_per_page' 		=> 10,
					'paged'					=> $paged,
					'cat' 					=> $cat,
			        'post_type'         	=> 'post',
			        'orderby'				=> 'date',
			        'order'           		=> 'desc',
			        'category__not_in'		=> array( 1 ),
			        'ignore_sticky_posts' 	=> true,
		        );

			}

		} else {

			$args = array(
				'posts_per_page' 		=> 10,
				'paged'					=> $paged,
				'author'				=> $author,
		        'post_type'         	=> 'post',
		        'orderby'				=> 'date',
		        'order'           		=> 'desc',
		        'category__not_in'		=> array( 1 ),
		        'ignore_sticky_posts' 	=> true,
	        );

		}

        $query = new WP_Query( $args );

        if ($query->have_posts()) {

       		$html = '';

       		$counter = 1;

       		while ( $query->have_posts() ) {

       			$query->the_post();

       			$featured = ( (is_sticky( get_the_id() )) ? 'featured' : '' );
       			
       			$thumb_id = get_post_thumbnail_id( get_the_id() );
				$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
       			$image = aq_resize( $thumb_url[0], 420 , 195 , true ); //resize & crop img
       			$thumb_url = $image;

       			$title = get_the_title();
       			$excerpt = get_the_excerpt();
       			$permalink = get_permalink( get_the_id() );

       			$tags = get_the_tags( get_the_id() );
       			
       			if ( !empty($tags) ) {
       				$tag_html = '<ul>';
       				foreach ($tags as $key => $tag) {
       					$tag_url = get_tag_link( $tag->term_id );
       					$tag_html .= '<li><a href="' . $tag_url . '">' . $tag->name . '</a></li>';
       				}
       				$tag_html .= '</ul>'; 
       			} else {
       				$tag_html = '';
       			}

       			$html .= '<div class="row item ' . $featured . '">
							<div class="col-sm-8 item-image">
								<img src="' . $thumb_url .  '" alt="">
							</div>
							<div class="col-sm-4 item-detail">
								<div class="item-title">
									<h4><a href="' . $permalink . '">' . $title . '</a></h4>
								</div>
								<div class="item-excerpt">
									' . $excerpt . '
								</div>
								<div class="item-read-more">
									<a href="' . $permalink .  '">READ ARTICLE &rarr;</a>
								</div>
								<div class="item-tag">
									' . $tag_html . '
								</div>
							</div>
						</div>';
				if ($counter++ %5 == 0) {
					$html .=  adrotate_ad(3);
				}
       		}

        } else {
        	$html = '';
        }
        echo $html;
	    exit;
	}

?>