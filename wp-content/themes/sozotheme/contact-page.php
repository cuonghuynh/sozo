<?php
/*
Template Name: Contact Us Page
*/
?>
<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="col-md-8 left-content">
					<div id="main-content" class="page">
						<div class="page-title"><?php echo get_the_title(); ?></div>
						<div class="page-content">
							<?php the_content(); ?>
						</div>
						<div class="share-box">
							<img src="images/share-box.jpg" alt="">
						</div>
						
					</div>		
				</div>	<!-- /main-content -->

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
