<?php get_header(); ?>

	<div class="col-md-8 left-content">
		<div id="main-content">


  <?php if (have_posts()) : ?>
	
	<div class="category-title">Search result.</div>	
	<?php 

		$count = 1;
		while (have_posts()) : the_post(); 

			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'post-thumb-normal', true);

	?>

				<div class="row item <?php if (is_sticky()) echo 'featured'; ?>">
				  <div class="col-sm-8 item-image">
					 <img src="<?php echo $thumb_url[0]; ?>" alt="">
				  </div>
				  <div class="col-sm-4 item-detail">
					 <div class="item-title">
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					 </div>
					 <div class="item-excerpt">
						<?php the_excerpt(); ?>
					 </div>
					 <div class="item-read-more">
						<a href="<?php the_permalink(); ?>">READ ARTICLE &rarr;</a>
					 </div>
					 <div class="item-tag">
						<ul>
						<?php 
						$tags = get_the_tags();
						?>
						<?php 
						  if (!empty($tags)) :
						  foreach ($tags as $key => $tag): 
						  $tag_link = get_tag_link( $tag->term_id );
						?>
						  <li><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a></li>
						<?php endforeach; endif; ?>
						</ul>
					 </div>
				  </div>
				</div> <!-- /item -->

				<?php if ($count++ % 3 == 0): ?>
					 <?php dynamic_sidebar( 'Post Content Sidebar' ); ?>
				<?php endif; ?>
	
		 <?php endwhile; ?>

  	<?php else : ?>
		
			<div class="category-title">No posts found.</div>	

  <?php endif; ?>
		  
		</div>  <!-- /main-content -->
	</div>

<?php get_footer(); ?>
