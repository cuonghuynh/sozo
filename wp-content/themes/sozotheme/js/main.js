$(document).ready(function() {

	WindowScroll_Event();

	ScrollTop_Click();

	SearchButton_Click();

	MobileMenuButton_Click();

	MobileSocialGroupButton_Click();

});

function WindowScroll_Event()
{
	$(window).scroll(function(event) {

		if ($(this).scrollTop() > 100) {

			$('#go-top').fadeIn();

		} else {

			$('#go-top').fadeOut();

		}

	});
}

function ScrollTop_Click()
{
	$('#go-top').click(function(event) {
		$('body, html').animate({scrollTop: 0}, 800);
		return false;
	});

}

function SearchButton_Click()
{
	$('#top-header .search-header').click(function(event) {
		/* Act on the event */
		if ($('#top-header .search-form').hasClass('show')) {
        	$('#top-header .search-form').removeClass('show');
		} else {
			$('#top-header .search-form').addClass('show');
		}
	});
		
}

function MobileMenuButton_Click()
{
	$('.mobile-menu-button').click(function(event) {
		/* Act on the event */

		if ( $('.social-group-mobile').hasClass('show') ) {
			$('.social-group-mobile').removeClass('show');
		}

		$('.menu-mobile').toggleClass('show');
	});
}

function MobileSocialGroupButton_Click()
{
	$('.mobile-social-group-button').click(function(event) {
		/* Act on the event */
		if ( $('.menu-mobile').hasClass('show') ) {
			$('.menu-mobile').removeClass('show');
		}

		$('.social-group-mobile').toggleClass('show');

	});
}