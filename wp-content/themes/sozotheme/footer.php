				<div class="col-md-4 right-content">
					<div id="sidebar">
						<div class="widget image">
							<div class="widget-content">
								<?php dynamic_sidebar( 'Right Sidebar' ); ?>
							</div>
						</div>	<!-- /widget image -->

						<div class="widget tab">
							<div class="widget-content">
								<ul class="nav nav-tabs">
									<li class="active">
										<a data-toggle="tab" href="#popular-post">POPULAR</a>
									</li>
									<li>
										<a data-toggle="tab" href="#latest-post">LATEST</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="popular-post" class="tab-pane fade in active">
										<?php include( TEMPLATEPATH . '/inc/popular_post.php'); ?>
									</div> <!-- /popular post -->
									<div id="latest-post" class="tab-pane fade">
										<?php include( TEMPLATEPATH . '/inc/latest_post.php' ); ?>
									</div>
								</div>
							</div>
						</div> <!-- /widget tab -->

						<div class="widget">
							<div class="widget-title">
								Editor's Pick
							</div>
							<div class="widget-content">
								<?php include( TEMPLATEPATH . '/inc/editor_pick.php' ); ?>
							</div>
						</div> <!-- /widget -->

						<div class="widget">
							<div class="widget-title">
								Best of Afachan
							</div>
							<div class="widget-content">
								<?php include( TEMPLATEPATH . '/inc/best_post.php' ); ?>
							</div>
						</div> <!-- /widget -->

						<div class="widget">
							<div class="widget-title">
								Culture
							</div>
							<div class="widget-content">
								<?php include( TEMPLATEPATH . '/inc/category_post.php' ); ?>
							</div>
						</div> <!-- /widget -->
					</div>
				</div> <!-- /sidebar -->

			</div>
		</div> <!-- /site-content -->

		<footer>
			<div class="footer-wrapper">
				<div class="row bottom-sidebar">
					<div class="col-md-2 col-sm-4 widget twitter-feed">
						<?php dynamic_sidebar( 'Twitter Feed Sidebar' ); ?>
					</div>
					<div class="col-md-2 col-sm-4 widget recent-post">
						<div class="widget-title">
							RECENT POSTS
						</div>
						<div class="widget-content">
							<?php include(TEMPLATEPATH . '/inc/recent_post.php'); ?>
						</div>	
					</div>
					<div class="col-md-2 col-sm-4 widget category">
						<div class="widget-title">
							CATEGORIES
						</div>	 
						<div class="widget-content">
							<ul class="category-list">
							<?php 
								$args = array(
								  'orderby' => 'name',
								  'parent' => 0,
								  'hide_empty' => 1,
								  'exclude' => 1,
								  );
								$categories = get_categories( $args );

							?>
								<?php foreach ($categories as $key => $cat): ?>
									<li>
										<a href="<?php echo get_category_link( $cat->cat_ID ); ?>"><?php echo $cat->name; ?></a>
									</li>
								<?php endforeach ?>
							</ul>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 widget like-us-on-facebook-box">
						<?php dynamic_sidebar( 'Fanpage Bottom Sidebar' ); ?>
					</div>
					<div class="col-md-3 col-sm-6 widget custom">
						<div class="widget-title">
							WIDGET CUSTOM
						</div>
						<div class="widget-content">
							<?php 
								$page = get_page( 2 );					
							?>
							<div class="thumb"><?php echo get_the_post_thumbnail( 2, 'full' ); ?></div>
							<div class="excerpt"><?php echo $page->post_excerpt; ?></div>
							
							<a class="read-more" href="<?php echo $page->guid; ?>">Read more</a>
							
							<?php 
								if ( function_exists( 'ot_get_option' ) ) {
									$logo 		= ot_get_option( 'logo' );
									$facebook 	= ot_get_option( 'facebook' );
									$twitter 	= ot_get_option( 'twitter' );
									$gplus 		= ot_get_option( 'google_plus' );
									$pinterest 	= ot_get_option( 'pinterest' );
									$vimeo 		= ot_get_option( 'vimeo' );
									$foursquare = ot_get_option( 'foursquare' );
									$rss 			= ot_get_option( 'rss' );
								}

							?>

							<div class="social-group">
								<ul>
									<?php if (!empty($rss)): ?>
										<li>
											<a href="<?php echo $rss; ?>"><i class="fa fa-rss"></i></a>
										</li>
									<?php endif ?>
									
									<?php if (!empty($facebook)): ?>
										<li>
											<a href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a>
										</li>
									<?php endif ?>
									
									<?php if (!empty($twitter)): ?>
										<li>
											<a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a>
										</li>
									<?php endif ?>
									
									<?php if (!empty($google_plus)): ?>
										<li>
											<a href="<?php echo $google_plus; ?>"><i class="fa fa-google-plus"></i></a>
										</li>
									<?php endif ?>
									
									<?php if (!empty($pinterest)): ?>
										<li>
											<a href="<?php echo $pinterest; ?>"><i class="fa fa-pinterest-square"></i></a>
										</li>
									<?php endif ?>
									
									<?php if (!empty($vimeo)): ?>
										<li>
											<a href="<?php echo $vimeo; ?>"><i class="fa fa-vimeo-square"></i></a>
										</li>
									<?php endif ?>
									
									<?php if (!empty($foursquare)): ?>
										<li>
											<a href="<?php echo $foursquare; ?>"><i class="fa fa-foursquare"></i></a>
										</li>
									<?php endif ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php 

				if ( function_exists( 'ot_get_option' ) ) {
					$copyright 		= ot_get_option( 'copyright_content' );
				}
			?>
			<div class="row copyright">
				<div class="copyright-wrapper">
					<div class="col-md-6 footer-menu">
						<?php 
							$menu = array('menu' => 'Page Navigation Menu');
							wp_nav_menu( $menu );
						?>
					</div>
					<div class="col-md-6 copyright-content">
						<?php echo $copyright; ?>
					</div>
				</div>	
			</div>
		</footer>
		<div id="go-top">
			<i class="fa fa-angle-up"></i>
		</div>
	</div> <!-- /site-wrapper -->

  <?php wp_footer(); ?>

  <!-- Don't forget analytics -->
	<!-- Bootstrap core JavaScript
	  ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo get_bloginfo( 'template_url' ); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo get_bloginfo( 'template_url' ); ?>/js/main.js"></script>
	
	<script src="https://apis.google.com/js/platform.js" async defer></script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=1786883151537434&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>


