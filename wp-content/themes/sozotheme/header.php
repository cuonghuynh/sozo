<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  
  	<?php if (is_search()) { ?>
	 <meta name="robots" content="noindex, nofollow" />
  	<?php } ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

  	<title>
		<?php
			if (function_exists('is_tag') && is_tag()) {
				single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
			elseif (is_archive()) {
				wp_title(''); echo ' Archive - '; }
			elseif (is_search()) {
				echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
			elseif (!(is_404()) && (is_single()) || (is_page())) {
				wp_title(''); echo ' - '; }
			elseif (is_404()) {
				echo 'Not Found - '; }
			if (is_home()) {
				bloginfo('name'); echo ' - '; bloginfo('description'); }
			else {
				 bloginfo('name'); }
			if ($paged>1) {
				echo ' - page '. $paged; }
		?>
  	</title>

  	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  	
  	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<!-- Bootstrap core CSS -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
	<!-- Optional Bootstrap Theme -->
	<link href="data:text/css;charset=utf-8," data-href="<?php echo get_bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet">
	<!-- Font Awesome  -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- Google - Roboto font -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:500,400italic,700italic,700,500italic,400' rel='stylesheet' type='text/css'>
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  	<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/modernizr-1.5.min.js"></script>
	

  	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

  	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	

	<?php 
		if ( function_exists( 'ot_get_option' ) ) {
			$logo 		= ot_get_option( 'logo' );
			$facebook 	= ot_get_option( 'facebook' );
			$twitter 	= ot_get_option( 'twitter' );
			$gplus 		= ot_get_option( 'google_plus' );
			$pinterest 	= ot_get_option( 'pinterest' );
			$vimeo 		= ot_get_option( 'vimeo' );
			$foursquare = ot_get_option( 'foursquare' );
			$rss 			= ot_get_option( 'rss' );
		}

	?>
  	<div id="site-wrapper">
		<div id="top-header">
			<div class="container visible-md visible-lg">
				<div class="top-menu">
					<?php 
						$menu = array('menu' => 'Page Navigation Menu');
						wp_nav_menu( $menu );
					?>
				</div>
				<div class="search-form">
					<?php include( TEMPLATEPATH . '/searchform.php' ); ?>
				</div>
				<div class="social-group">
					<ul>
						<?php if (!empty($rss)): ?>
							<li>
								<a href="<?php echo $rss; ?>"><i class="fa fa-rss"></i></a>
							</li>
						<?php endif ?>
						
						<?php if (!empty($facebook)): ?>
							<li>
								<a href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a>
							</li>
						<?php endif ?>
						
						<?php if (!empty($twitter)): ?>
							<li>
								<a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a>
							</li>
						<?php endif ?>
						
						<?php if (!empty($google_plus)): ?>
							<li>
								<a href="<?php echo $google_plus; ?>"><i class="fa fa-google-plus"></i></a>
							</li>
						<?php endif ?>
						
						<?php if (!empty($pinterest)): ?>
							<li>
								<a href="<?php echo $pinterest; ?>"><i class="fa fa-pinterest-square"></i></a>
							</li>
						<?php endif ?>
						
						<?php if (!empty($vimeo)): ?>
							<li>
								<a href="<?php echo $vimeo; ?>"><i class="fa fa-vimeo-square"></i></a>
							</li>
						<?php endif ?>
						
						<?php if (!empty($foursquare)): ?>
							<li>
								<a href="<?php echo $foursquare; ?>"><i class="fa fa-foursquare"></i></a>
							</li>
						<?php endif ?>
						<li>
							<a href="#" class="search-header"><i class="fa fa-search"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="container visible-xs visible-sm mobile">
				<div class="mobile-menu-button">
					<i class="fa fa-bars"></i>
				</div>
				<div class="mobile-search-form">
					<?php include( TEMPLATEPATH . '/searchform.php' ); ?>
				</div>
				<div class="mobile-social-group-button">
					<i class="fa fa-globe"></i>
				</div>
			</div>
		</div> <!-- /top-header -->
		<div class="menu-mobile">
			<div class="main-menu-mobile">
				<h4>Main Menu</h4>
				<?php 
					$menu = array('menu' => 'Category Navigation Menu');
					wp_nav_menu( $menu );
				?>
			</div>
			<div class="main-nav">
				<h4>About Us</h4>
				<?php 
					$menu = array('menu' => 'Page Navigation Menu');
					wp_nav_menu( $menu );
				?>
			</div>
		</div> <!-- /menu-mobile -->
		<div class="social-group-mobile">
			<ul>
				<?php if (!empty($rss)): ?>
					<li>
						<a href="<?php echo $rss; ?>"><i class="fa fa-rss"></i></a>
					</li>
				<?php endif ?>
				
				<?php if (!empty($facebook)): ?>
					<li>
						<a href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a>
					</li>
				<?php endif ?>
				
				<?php if (!empty($twitter)): ?>
					<li>
						<a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a>
					</li>
				<?php endif ?>
				
				<?php if (!empty($google_plus)): ?>
					<li>
						<a href="<?php echo $google_plus; ?>"><i class="fa fa-google-plus"></i></a>
					</li>
				<?php endif ?>
				
				<?php if (!empty($pinterest)): ?>
					<li>
						<a href="<?php echo $pinterest; ?>"><i class="fa fa-pinterest-square"></i></a>
					</li>
				<?php endif ?>
				
				<?php if (!empty($vimeo)): ?>
					<li>
						<a href="<?php echo $vimeo; ?>"><i class="fa fa-vimeo-square"></i></a>
					</li>
				<?php endif ?>
				
				<?php if (!empty($foursquare)): ?>
					<li>
						<a href="<?php echo $foursquare; ?>"><i class="fa fa-foursquare"></i></a>
					</li>
				<?php endif ?>
			</ul>
		</div> <!-- /social-group-mobile -->
		<header class="container">
			<div class="row">
				<div id="logo-wrapper">
					<div class="col-md-4">
						<?php if (!empty($logo)): ?>
							<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo $logo; ?>" alt=""></a>
						<?php else: ?>
							<a href="<?php echo home_url(); ?>" class="logo"><img src="images/logo.png" alt=""></a>
						<?php endif ?>

					</div>
					<div class="col-md-8">
						<div class="adverts-wrapper">
							<?php dynamic_sidebar( 'Banner Sidebar' ); ?>
						</div>
					</div>
				</div>
			</div>
			<nav class="main-menu">
				<?php 
					$menu = array('menu' => 'Category Navigation Menu');
					wp_nav_menu( $menu );
				?>
			</nav> <!-- /main-menu -->
		</header> <!-- /header -->
		
		<div id="site-content" class="container">
			<div class="row">