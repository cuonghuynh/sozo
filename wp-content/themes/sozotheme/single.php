<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  				<div class="col-md-8 left-content">
					<div id="main-content" class="article">
						<div class="page-title">
							<h1><?php the_title(); ?></h1>
							<div class="meta">
								Written by <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php the_author(); ?></a>, <?php the_time( 'F d, Y' ); ?>
							</div>
							<div class="tag">
								<ul>
								<?php 
									$tags = get_the_tags();
								?>
								<?php 
									if (! empty($tags)) :
									foreach ($tags as $key => $tag): 
									$tag_link = get_tag_link( $tag->term_id );
								?>
									<li><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a></li>
								<?php endforeach; endif; ?>
								</ul>
							</div>
						</div>
						<div class="page-content">
							<?php the_content(); ?>
						</div>
						<?php include(TEMPLATEPATH . '/inc/nav.php'); ?>
						<div class="comment">
							<?php dynamic_sidebar( 'Facebook Comment Sidebar' ); ?>
						</div>
					</div>
				
				</div>	<!-- /main-content -->

<?php endwhile; endif; ?>

<?php get_footer(); ?>