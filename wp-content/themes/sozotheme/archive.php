<?php get_header(); 
?>

	<?php if (have_posts()) : ?>

	 <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

	 <?php /* If this is a category archive */ if (is_category()) { ?>
		<div class="category-title"><?php single_cat_title(); ?></div>

	 <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<div class="category-title">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</div>

	 <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<div class="category-title">Archive for <?php the_time('F jS, Y'); ?></div>

	 <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<div class="category-title">Archive for <?php the_time('F, Y'); ?></div>

	 <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<div class="category-title">Archive for <?php the_time('Y'); ?></div>

	 <?php /* If this is an author archive */ } elseif (is_author()) { ?>

	 <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<div class="category-title">Blog Archives</div>

	 <?php } ?>
	 
		  <div class="col-md-8 left-content">
			 <div id="main-content">
			<?php 

				if (is_author()) {

				$user_id = get_query_var( 'author' );
				$author_name = get_author_name( $user_id ); 

				$user = wp_get_current_user();
				

		
				?>

					  <?php dynamic_sidebar( 'Author Sidebar' ); ?>
					</div>
			<?php	} ?>
  <?php 

  	$paged = 1;

	$count = 1;
	 while (have_posts()) : the_post(); 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src( $thumb_id, 'full' );

		$image = aq_resize( $thumb_url[0], 420 , 195 , true ); //resize & crop img

  ?>
	 
				<div class="row item <?php if (is_sticky()) echo 'featured'; ?>">
				  <div class="col-sm-8 item-image">
					 <img src="<?php echo $image; ?>" alt="">
				  </div>
				  <div class="col-sm-4 item-detail">
					 <div class="item-title">
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					 </div>
					 <div class="item-excerpt">
						<?php the_excerpt(); ?>
					 </div>
					 <div class="item-read-more">
						<a href="<?php the_permalink(); ?>">READ ARTICLE &rarr;</a>
					 </div>
					 <div class="item-tag">
						<ul>
						<?php 
						$tags = get_the_tags();
						?>
						<?php 
						  if (!empty($tags)) :
						  foreach ($tags as $key => $tag): 
						  $tag_link = get_tag_link( $tag->term_id );
						?>
						  <li><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a></li>
						<?php endforeach; endif; ?>
						</ul>
					 </div>
				  </div>
				</div> <!-- /item -->

				<?php if ($count++ % 5 == 0): ?>
					 <?php dynamic_sidebar( 'Post Content Sidebar' ); ?>
				<?php endif; ?>

	 <?php endwhile; ?>
	 
			 

	<?php else : ?>

				<h1>Nothing found</h1>

			

	<?php endif; ?>

			</div>

			<div id="load-more-post" data-paged="<?php echo $paged; ?>" data-cat="<?php echo get_query_var('cat'); ?>" data-author="<?php echo get_query_var('author'); ?>">
				<h4><img src="<?php echo get_bloginfo('template_url'); ?>/images/ajax-loader.gif" alt="">Load more post</h4>
			</div>
		</div>  <!-- /main-content -->

<?php get_footer(); ?>
